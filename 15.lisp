(defun ! (n)
  (reduce #'* (loop for x from 1 to n collect x)))

(defun problem15 ()
  (/ (! 40) (! 20) (! 20)))

(time (print (problem15)))
