(defun problem16 ()
  (let ((str (write-to-string (expt 2 1000)))
		(sum 0))
	(dotimes (i (length str))
	  (setq sum (+ sum (parse-integer (subseq str i (1+ i))))))
	(print sum)))

(time (problem16))
